//
//  HistorialViewController.swift
//  SplashScreen
//
//  Created by Jorge Romo Gonzalez on 5/11/17.
//  Copyright © 2017 Ulsa. All rights reserved.
//

import UIKit

class HistorialViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        estiloVisual()
        
        
        // Do any additional setup after loading the view.
    }

    
    func estiloVisual(){
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0/255, green: 29/255, blue: 104/255, alpha: 1)
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.slideMenuController()?.removeRightGestures()
        
        self.title = "Historial"
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.setNavigationBarItem()
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
