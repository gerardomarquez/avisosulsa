//
//  Alert.swift
//  SplashScreen
//
//  Created by Ulsa on 5/19/17.
//  Copyright © 2017 Ulsa. All rights reserved.
//

import Foundation
import ObjectMapper

class Json:Mappable {
    var alert: [Alert]
    required init?(map:Map){  // Se requiere inicializar
    }
    func mapping(map:Map){
        alert <- map[Alert]
    }
    
}

class Alert:Mappable{
    var id:String?
    var titulo:String?
    var mensaje:String?
    var imagen:String?
    
    required init?(map:Map){  // Se requiere inicializar
    }
    
    func mapping(map:Map){
        id <- map["id"]
        titulo <- map["titulo"]
        mensaje <- map["mensaje"]
        imagen <- map["imagen"]
    }
}
