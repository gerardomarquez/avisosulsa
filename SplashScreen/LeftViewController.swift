//
//  LeftViewController.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 12/3/14.
//

import UIKit

enum LeftMenu: Int {
    case main = 0
    case historial
    case contacto
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class LeftViewController : UIViewController, LeftMenuProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    var menus = ["Home", "Historial", "Contacto"]
    var mainViewController: UIViewController!
    var historialViewController: UIViewController!
    var contactoViewController: UIViewController!
    var homeViewController: UIViewController!


    
    @IBAction func salir(_ sender: Any) {
        self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorColor = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let historialViewController = storyboard.instantiateViewController(withIdentifier: "HistorialViewController") as! HistorialViewController
        
        self.historialViewController = UINavigationController(rootViewController: historialViewController)
        
        let contactoViewController = storyboard.instantiateViewController(withIdentifier: "ContactoViewController") as! ContactoViewController
        self.contactoViewController = UINavigationController(rootViewController: contactoViewController)
        

        let homeViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        
        self.homeViewController = UINavigationController(rootViewController: homeViewController)
        
        
        
        
        self.tableView.registerCellClass(BaseTableViewCell.self)
        
        
    }
    
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        tableView.reloadData()
        tableView.allowsMultipleSelection = false
        //tableView.cellForRowAtIndexPath(indexPath)!.backgroundColor = UIColor.grayColor()
        
        
        
        
        
        
        if let menu = LeftMenu(rawValue: indexPath.item) {
            self.changeViewController(menu)
            
        }
    }
    
    
    
    
    
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.layoutIfNeeded()
    }
    
    func changeViewController(_ menu: LeftMenu) {
        switch menu {
        case .main:
            self.slideMenuController()?.changeMainViewController(self.homeViewController, close: true)
        case .historial:
            self.slideMenuController()?.changeMainViewController(self.historialViewController, close: true)
        case .contacto:
            self.slideMenuController()?.changeMainViewController(self.contactoViewController, close: true)

            
            
        }
    }
}

extension LeftViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .main, .historial, .contacto:
                return BaseTableViewCell.height()
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView == scrollView {
            
        }
    }
}

extension LeftViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let menu = LeftMenu(rawValue: indexPath.row) {
            
            
            
            switch menu {
            case .main, .historial, .contacto:
                let cell = BaseTableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: BaseTableViewCell.identifier)
                cell.setData(menus[indexPath.row])
                cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
                cell.textLabel?.textColor = UIColor.gray
                cell.selectionStyle = .none
                cell.backgroundColor = UIColor.clear
                cell.textLabel?.text = menus[indexPath.row]
                
                
                switch indexPath.row {
                case 0:  cell.imageView?.image = UIImage(named: "shape")
                cell.imageView?.contentMode = UIViewContentMode.center
                case 1: cell.imageView?.image = UIImage(named: "rectangle")
                cell.imageView?.contentMode = UIViewContentMode.center
                case 2: cell.imageView?.image = UIImage(named: "dollar")
                cell.imageView?.contentMode = UIViewContentMode.center
                case 3: cell.imageView?.image = UIImage(named: "fuel")
                cell.imageView?.contentMode = UIViewContentMode.center
                case 4: cell.imageView?.image = UIImage(named: "settings")
                cell.imageView?.contentMode = UIViewContentMode.center
                case 5: cell.imageView?.image = UIImage(named: "about")
                cell.imageView?.contentMode = UIViewContentMode.center
                    
                    
                    
                    
                    
                default: print("otro")
                }
                
                return cell
            }
        }
        return UITableViewCell()
    }
    
    
}
