//
//  HomeViewController.swift
//  SplashScreen
//
//  Created by Ulsa on 3/23/17.
//  Copyright © 2017 Ulsa. All rights reserved.
//

import UIKit
import Alamofire

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        Alamofire.request("http://echo.jsontest.com/key/value/one/two", method: .get, parameters: nil, encoding: JSONEncoding.default)
            
            .responseJSON { response in
                
                print("alamofire")
                
                print(response.request as Any)  // original URL request
                
                print(response.response as Any) // URL response
                
                print(response.result.value as Any)   // result of response serialization
                
                
                
                print("alamofire en activo")
                
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
