//
//  ViewController.swift
//  SplashScreen
//
//  Created by Ulsa on 3/2/17.
//  Copyright © 2017 Ulsa. All rights reserved.
//

import UIKit
import IJProgressView
import Alamofire

class LoginViewController: UIViewController {
    var mainViewController: UIViewController!
    weak var delegate: LeftMenuProtocol?



    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0/255, green: 29/255, blue: 104/255, alpha: 1)
        self.title = "Avisos ULSA"
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.black.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.8
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2
        btnInicio.layer.cornerRadius = 7
        btnInicio.layer.borderWidth = 1
        btnInicio.layer.borderColor = UIColor.clear.cgColor
    }
    
    @IBOutlet weak var txtCorreo: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBAction func btnIniciarSesion(_ sender: Any) {
        let usuario = "Gerardo"
        let password = "123456"
        Alamofire.request("http://175.45.2.36/slimtest/public/inicio", method: .post, parameters: ["correo":txtCorreo.text!,"password":txtPassword.text!], encoding: JSONEncoding.default)
            .responseJSON { response in
                print("alamofire")
                print(response.request as Any)  // original URL request
                print(response.response as Any) // URL  response
                print(response.result.value as! String)   // result of response serialization
                print("alamofire en activo")
                if response.result.value as! String == "true"{
                    let mainViewController = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                    self.mainViewController = UINavigationController(rootViewController: mainViewController)
                    mainViewController.delegate = self.delegate
                    self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
                }else{
                    let alert = UIAlertController(title: "Error", message: "Verifica tus datos.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
        }
//        if txtCorreo.text == "Gerardo" && self.txtPassword.text == "123456" {
//            
//            
//            let mainViewController = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
//            
//            self.mainViewController = UINavigationController(rootViewController: mainViewController)
//            mainViewController.delegate = self.delegate
//            
//            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
//            
//            
//       // IJProgressView.shared.showProgressView(view)
//       // self.performSegue(withIdentifier: "Home", sender: self)
//        }else{
//        let alert = UIAlertController(title: "Error", message: "Verifica tus datos.", preferredStyle: UIAlertControllerStyle.alert)
//        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//        self.present(alert, animated: true, completion: nil)
//    }
    }
        
    
    override func motionBegan(_ motion: UIEventSubtype, with event: UIEvent?) {
        let alert = UIAlertController(title: "Alerta", message: "Devive was shaken!", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    @IBOutlet weak var btnInicio: UIButton!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //IJProgressView.shared.hideProgressView()
    }
    

                
    
}

