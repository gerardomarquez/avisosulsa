//
//  MainViewController.swift
//  SplashScreen
//
//  Created by Jorge Romo Gonzalez on 5/11/17.
//  Copyright © 2017 Ulsa. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import Alamofire
import AlamofireObjectMapper



class MainViewController: UIViewController {

    weak var delegate: LeftMenuProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        estiloVisual()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func estiloVisual(){
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0/255, green: 29/255, blue: 104/255, alpha: 1)

        self.navigationController?.navigationBar.isTranslucent = false
        self.slideMenuController()?.removeRightGestures()
        
        self.title = "Home"
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.setNavigationBarItem()
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Alamofire.request("http://175.45.2.36/slimtest/public/mensajes", method: .get, parameters: nil)
            
            .responseArray { (response: DataResponse<[Json]>) in
                if let result = response.result.value{
                    
                    for i in result {
                        print(i.)
                    }
                }
                
                print("alamofire")
                print(response.request as Any)  // original URL request
                print(response.response as Any) // URL response
                print(response.result.value as Any)   // result of response serialization
                print("alamofire en activo")
                
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MainViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
        
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
