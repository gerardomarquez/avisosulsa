//
//  ContactoViewController.swift
//  SplashScreen
//
//  Created by Jorge Romo Gonzalez on 5/11/17.
//  Copyright © 2017 Ulsa. All rights reserved.
//

import UIKit
import MapKit
import MessageUI


class ContactoViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    
    @IBAction func Ir(_ sender: Any) {
        
        //Define el destino
        
        let latitud:CLLocationDegrees = 28.608838
        let longitud: CLLocationDegrees = -106.125444
        
        let regionDistance:CLLocationDistance = 1000;
        let coordinates = CLLocationCoordinate2DMake(latitud, longitud)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        
        let options = [MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center), MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)]
        
        let placemark = MKPlacemark(coordinate: coordinates)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "ULSA"
        mapItem.openInMaps(launchOptions: options)
        
    }
    
    
    @IBAction func enviarCorreo(_ sender: Any) {
        
        let mailCompese = MFMailComposeViewController()
        mailCompese.mailComposeDelegate = self
        mailCompese.setToRecipients(["admision@ulsachihuahua.edu.mx"])
        mailCompese.setSubject("")
        mailCompese.setMessageBody("", isHTML: false)
        
        if MFMailComposeViewController.canSendMail()
        {
            self.present(mailCompese, animated: true, completion: nil)
        }
        else
        {
                print("!!!!!")
        }
    
        
    }
    
    func mailComposeController( _ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        controller.dismiss(animated: true, completion: nil)
    }
    @IBAction func llamar(_ sender: Any)
    
    {
        let url: NSURL = URL(string: "TEL://6144321464")! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    
    
    }
    

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        estiloVisual()
        
        // Do any additional setup after loading the view.
    }
    
    
    
    

    func estiloVisual(){
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0/255, green: 29/255, blue: 104/255, alpha: 1)
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.slideMenuController()?.removeRightGestures()
        
        self.title = "Contacto"
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.setNavigationBarItem()
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
